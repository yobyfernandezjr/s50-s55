import { Fragment,useState,useEffect,useContext } from 'react'
import { Form, Button } from 'react-bootstrap'
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

  const navigate = useNavigate();
  const {user} = useContext(UserContext);
  
  //state hook
  const[firstName, setFirstName] = useState("");
  const[lastName, setLastName] = useState("");
  const[mobileNo, setMobileNo] = useState("");
  const[email, setEmail] = useState(""); 
  const [password1,setPassword1] = useState("");
  const [password2,setPassword2] = useState("");

  
  //state to determine whether submit button is enable or not
  const [isActive, setIsActive] = useState(false);


//Check if values are successfully passed
// console.log(`firstName: ${firstName}`);
// console.log(`lastName: ${lastName}`);
// console.log(`mobileNumber: ${mobileNo}`);
// console.log(`Email: ${email}`);
// console.log(`Password1: ${password1}`);
// console.log(`Password2: ${password2}`);
// console.log(email);
// console.log(password1);
// console.log(password2);

  function registerUser(e) { 
    
    e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json',
			},
			body: JSON.stringify({
				email: email
			})

		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			if(data){
				Swal.fire({
					title: "Registration failed!",
					icon: "error",
					text: "Please provide different email."
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json',
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo,
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data === true){
						Swal.fire({
							title: 'Registration Successful!',
							icon: 'success',
							text: 'Thank you for registering'
						})

								// clear input fields
	              		setFirstName("");
						setLastName("");
						setEmail("");
						setMobileNo("");
						setPassword1("");
						setPassword2("");

						navigate('/login')
					} else {
						Swal.fire({
							title: 'Registration failed!',
							icon: 'error',
							text: 'Please try again.'
						})
					}
				})
			}
		})
		//alert('Thank you for registering');
	};

    
    

  



  //useEffect for button
  useEffect(() => {
    
    if((firstName !== '' && lastName !== '' && mobileNo.length === 11 && email !== '' && password1 !== '' && password2 !== '') && (  password1 === password2)){
        setIsActive(true);
    } else {
        setIsActive(false);
    }
}, [firstName, lastName, mobileNo, email, password1, password2]);


  
  
  
  
    return (
      (user.id !== null) ?
      <Navigate to="/login"/>
  
      :
    <Fragment>

    <h1 className='text-center mt-3'>Register</h1>
    <Form onSubmit={(e) => {registerUser(e)}}>
{/* FirstName */}
    <Form.Group className="mb-3" controlId="firstName">
        <Form.Label>First Name </Form.Label>
        <Form.Control
					type="text"
                    value = {firstName}
					placeholder="Enter your first name here"
					onChange = {(e) => {setFirstName(e.target.value)}}
                    required
				/>

      </Form.Group>

  {/* LastName */}
     <Form.Group className="mb-3" controlId="lastName">
        <Form.Label>Last Name </Form.Label>
        <Form.Control
					type="text"
                    value = {lastName}
					placeholder="Enter your last name here"
					onChange = {(e) => {setLastName(e.target.value)}}
                    required
				/>

      </Form.Group>

      {/* mobilenumber */}
    <Form.Group className="mb-3" controlId="mobileNo">
        <Form.Label>Mobile Number </Form.Label>
        <Form.Control
					type="text"
                    value = {mobileNo}
					placeholder="09176543210"
					onChange = {(e) => {setMobileNo(e.target.value)}}
                    required
				/>

      </Form.Group>





      <Form.Group className="mb-3" controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
					type="email"
                    value = {email}
					placeholder="Enter your email here"
					onChange = {(e) => {setEmail(e.target.value)}}
                    required
				/>

        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password">
        <Form.Label>Password</Form.Label>
        <Form.Control
					type="password"
                    value = {password1}
					placeholder="Enter your Password"
					onChange = {(e) => {setPassword1(e.target.value)}}
                    required
				/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
					type="password"
                    value = {password2}
					placeholder="Enter your Password Again"
					onChange = {(e) => {setPassword2(e.target.value)}}
                    required
				/>
      </Form.Group> 
     { isActive ?
        <Button variant="success" type='submit' id='submitBtn' >
        Submit
      </Button>
        :
        <Button variant="primary" type='submit' id='submitBtn' disabled>
        Submit
      </Button>

    }
     
    </Form>
    </Fragment>
  )
}
