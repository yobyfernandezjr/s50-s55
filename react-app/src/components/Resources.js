import Accordion from 'react-bootstrap/Accordion';
import Button from 'react-bootstrap/Button';

function FlushExample() {
  return (
    <Accordion>
      <Accordion.Item eventKey="0" className = "p-1 mt-3">
        <Accordion.Header>Angular vs. React in 2022</Accordion.Header>
        <Accordion.Body>
          Over the last decade, the number of JavaScript frameworks has increased dramatically. Angular & React have emerged as popular choices among the top JavaScript frameworks. They allow developers to create high-quality web apps and mobile apps in less time & with less code than ever before possible.
          <a href="https://enlear.academy/angular-vs-react-in-2022-83a2ac15230c" target="_blank">
          <Button variant="primary" size="sm">
          Read more
          </Button></a>
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="1" className = "p-1 mt-3">
        <Accordion.Header>Exciting New CSS Features in 2022</Accordion.Header>
        <Accordion.Body>
          CSS has changed rapidly in recent years, but I think 2021 is a booming year for CSS with many new features. Such as CSS container query, CSS parent selector, CSS cascade control rules, CSS grid, etc. And all major browser manufacturers are excited about this feature.
          It is a boon to our developers, let us thank them for their hard work!
          Let’s take a look at what’s new in CSS in 2022.
          <a href="https://enlear.academy/4-exciting-new-css-features-in-2022-6a4e06552adb" target="_blank">
          <Button variant="primary" size="sm">
          Read More
          </Button></a>
        </Accordion.Body>
      </Accordion.Item>
      <Accordion.Item eventKey="2" className = "p-1 mt-3">
        <Accordion.Header>Top 10 Skills A Frontend Developer Must Have In 2022</Accordion.Header>
        <Accordion.Body>
          A front-end developer works on a website’s and application’s user interface. They are in charge of determining the structure and design of the web application, adding features to improve user experience, creating a balance between design and functionality. Optimizing the web application for various devices, optimizing pages speed and scalability, coding web pages using multiple markup languages. It also helps maintain brand consistency and write the codes used again.
          <a href="https://enlear.academy/top-10-skills-a-frontend-developer-must-have-in-2022-7856c4b5af1f" target="_blank">
          <Button variant="primary" size="sm">
          Read More
          </Button></a>
        </Accordion.Body> 
      </Accordion.Item>
    </Accordion>
  );
}

export default FlushExample;