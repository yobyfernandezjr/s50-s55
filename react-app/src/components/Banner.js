import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner ({data}) {
	// console.log(data)
	const { title, content, destination, label } = data;

	return (
		<Row>
			<Col className="p-5">
				<h1>{title}</h1>
				<p>{content}</p>
				<Button variant="primary" as={Link} to={destination}>{label}</Button>
			</Col>
		</Row>
	)
};

// export default function Banner() {
// 	return (
// 		<Row>
// 			<Col className="p-5">
// 				<h1>Zuitt Coding Bootcamp</h1>
// 				<p>Opportunities for everyone, everywhere.</p>
// 				<Button variant="primary">Enroll now!</Button>
// 			</Col>
// 		</Row>
// 	)
// };

// export const PageNotFound = () => {
// 	return (
// 		<>
// 			<h1> 404 Error </h1>
// 			<h1> Page Not Found </h1>
// 			<Button variant="primary">Go to homepage</Button>
// 		</>
// 	)
// };
